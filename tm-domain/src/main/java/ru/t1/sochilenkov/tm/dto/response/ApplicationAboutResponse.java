package ru.t1.sochilenkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationAboutResponse extends AbstractResponse {

    private String email;

    private String name;

}
