package ru.t1.sochilenkov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskStartByIndexResponse extends AbstractTaskResponse {

    public TaskStartByIndexResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
