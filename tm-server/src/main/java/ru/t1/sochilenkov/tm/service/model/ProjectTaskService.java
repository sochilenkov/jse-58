package ru.t1.sochilenkov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.sochilenkov.tm.api.repository.model.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.model.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.model.IProjectTaskService;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.IndexIncorrectException;
import ru.t1.sochilenkov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.TaskIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.UserIdEmptyException;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    private ITaskRepository getTaskRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @NotNull final EntityManager entityManagerTask = taskRepository.getEntityManager();
        @Nullable final Task task;
        @Nullable final Project project;
        try {
            project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProject(project);
            entityManagerTask.getTransaction().begin();
            taskRepository.update(task);
            entityManagerTask.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerTask.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @NotNull final EntityManager entityManagerTask = taskRepository.getEntityManager();
        @NotNull final List<Task> tasks;
        try {
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            tasks = taskRepository.findAllByProjectId(userId, projectId);
            entityManagerTask.getTransaction().begin();
            for (final Task task : tasks) taskRepository.removeById(userId, task.getId());
            entityManagerTask.getTransaction().commit();
            entityManager.getTransaction().begin();
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerTask.close();
        }
    }

    @Override
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex == null || projectIndex < 1) throw new IndexIncorrectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @NotNull final EntityManager entityManagerTask = taskRepository.getEntityManager();
        @NotNull final List<Task> tasks;
        @Nullable Project project;
        try {
            project = projectRepository.findOneByIndex(userId, projectIndex);
            if (project == null) throw new ProjectNotFoundException();
            tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManagerTask.getTransaction().begin();
            for (final Task task : tasks) taskRepository.removeById(userId, task.getId());
            entityManagerTask.getTransaction().commit();
            entityManager.getTransaction().begin();
            projectRepository.removeById(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerTask.close();
        }
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @NotNull final EntityManager entityManagerTask = taskRepository.getEntityManager();
        @Nullable final Task task;
        try {
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProject(null);
            entityManagerTask.getTransaction().begin();
            taskRepository.update(task);
            entityManagerTask.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerTask.close();
        }
    }

}
